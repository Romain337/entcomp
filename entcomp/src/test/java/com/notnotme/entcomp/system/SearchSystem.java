package com.notnotme.entcomp.system;

import com.notnotme.entcomp.Component;
import com.notnotme.entcomp.EntityManager;
import com.notnotme.entcomp.System;
import com.notnotme.entcomp.components.Name;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public final class SearchSystem implements System {

    private EntityManager mEntityManager;

    @Override
    public void onStart(EntityManager entityManager) {
        mEntityManager = entityManager;
    }

    @Override
    public void onStop(EntityManager entityManager) {
        mEntityManager = null;
    }

    @Override
    public String getName() {
        return SearchSystem.class.getSimpleName();
    }

    public List<UUID> searchByName(String name) {
        ArrayList<UUID> result = new ArrayList<>();
        for (UUID entity : mEntityManager.getAllEntities(Name.class)) {
            Name entityName = mEntityManager.getComponent(entity, Name.class);
            if (entityName.name.toLowerCase().contains(name.toLowerCase())) result.add(entity);
        }

        return result;
    }

}
