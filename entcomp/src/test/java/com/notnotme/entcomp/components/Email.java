package com.notnotme.entcomp.components;

import com.notnotme.entcomp.Component;

public final class Email implements Component {

    public String email;

    public Email() {
        email = "No email";
    }

    public Email(String email) {
        this.email = email;
    }

}