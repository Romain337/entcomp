package com.notnotme.entcomp.components;

import com.notnotme.entcomp.Component;

public final class PhoneNumber implements Component {

    public String phone;

    public PhoneNumber() {
        phone = "No phone number";
    }

    public PhoneNumber(String phone) {
        this.phone = phone;
    }

}