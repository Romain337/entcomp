package com.notnotme.entcomp.components;

import com.notnotme.entcomp.Component;

public final class Name implements Component {

    public String name;

    public Name() {
        name = "No name";
    }

    public Name(String name) {
        this.name = name;
    }

}