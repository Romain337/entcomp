package com.notnotme.entcomp;

import com.notnotme.entcomp.components.Email;
import com.notnotme.entcomp.components.Name;
import com.notnotme.entcomp.components.PhoneNumber;
import com.notnotme.entcomp.system.SearchSystem;

import org.junit.Test;

import java.lang.*;
import java.lang.System;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

public class EntCompUnitTest {

    @Test
    public void entityManager_Test() throws Exception {
        EntComp entComp = new EntComp();
        entComp.getSystemManager().addSystem(new SearchSystem());
        entComp.initialize();

        EntityManager entityManager = entComp.getEntityManager();
        for (int i=0; i<50000; i++) {
            UUID contact = entityManager.createEntity();
            if (i % 2 == 0) entityManager.addComponent(contact, new Name(contact.toString()));
            if (i % 4 == 0) entityManager.addComponent(contact, new Email(String.valueOf(i)));
            if (i % 8 == 0) entityManager.addComponent(contact, new PhoneNumber(String.valueOf(10000-i)));
        }

        SearchSystem searchSystem = entComp.getSystemManager().getSystem(SearchSystem.class);

        long time = System.currentTimeMillis();
        assertEquals(searchSystem.searchByName("Contact 1").size(), 0);
        time = System.currentTimeMillis() - time;
        System.out.println("SearchTime (millis): " + time);

        time = System.currentTimeMillis();
        assertEquals(searchSystem.searchByName("Contact 2").size(), 0);
        time = System.currentTimeMillis() - time;
        System.out.println("SearchTime (millis): " + time);

        time = System.currentTimeMillis();
        assertEquals(searchSystem.searchByName("Blah").size(), 0);
        time = System.currentTimeMillis() - time;
        System.out.println("SearchTime (millis): " + time);

        entComp.destroy();
    }

}