package com.notnotme.entcomp;

import com.notnotme.entcomp.components.Email;
import com.notnotme.entcomp.components.Name;
import com.notnotme.entcomp.components.PhoneNumber;
import com.notnotme.entcomp.system.SearchSystem;

import org.junit.Test;

import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class SystemManagerUnitTest {

    @Test
    public void systemManager_Test() throws Exception {
        EntityManager entityManager = new EntityManager();

        UUID contact1 = entityManager.createEntity();
        entityManager.addComponent(contact1, new Name("Contact 1"));
        entityManager.addComponent(contact1, new Email("Contact1@youpi.com"));

        UUID contact2 = entityManager.createEntity();
        entityManager.addComponent(contact2, new Name("Contact 2"));
        entityManager.addComponent(contact2, new PhoneNumber("+33686956345"));

        UUID contact3 = entityManager.createEntity();
        entityManager.addComponent(contact3, new Name("Contact 3"));
        entityManager.addComponent(contact3, new Email("Contact3@youpi.com"));
        entityManager.addComponent(contact3, new PhoneNumber("+33686956345"));

        SearchSystem searchSystem = new SearchSystem();
        searchSystem.onStart(entityManager);

        SystemManager systemManager = new SystemManager();
        systemManager.addSystem(searchSystem);
        assertNotEquals(systemManager.getSystem(SearchSystem.class), null);

        systemManager.removeSystem(SearchSystem.class);
        assertEquals(systemManager.getCount(), 0);

        assertEquals(searchSystem.searchByName("Contact 1").size(), 1);
        assertEquals(searchSystem.searchByName("Contact 2").size(), 1);
        assertEquals(searchSystem.searchByName("Blah").size(), 0);

        searchSystem.onStop(entityManager);
    }

}