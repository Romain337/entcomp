package com.notnotme.entcomp;

import com.notnotme.entcomp.components.Email;
import com.notnotme.entcomp.components.Name;
import com.notnotme.entcomp.components.PhoneNumber;

import org.junit.Test;

import java.util.UUID;

import static org.junit.Assert.assertEquals;

public class EntityManagerUnitTest {

    @Test
    public void entityManager_Test() throws Exception {
        EntityManager entityManager = new EntityManager();

        UUID contact1 = entityManager.createEntity();
        entityManager.addComponent(contact1, new Name("Contact 1"));
        entityManager.addComponent(contact1, new Email("Contact1@youpi.com"));

        UUID contact2 = entityManager.createEntity();
        entityManager.addComponent(contact2, new Name("Contact 2"));
        entityManager.addComponent(contact2, new PhoneNumber("+33686956345"));

        UUID contact3 = entityManager.createEntity();
        entityManager.addComponent(contact3, new Name("Contact 3"));
        entityManager.addComponent(contact3, new Email("Contact3@youpi.com"));
        entityManager.addComponent(contact3, new PhoneNumber("+33686956345"));

        assertEquals(entityManager.getCount(), 3);
        assertEquals(entityManager.getAllEntities(Name.class).size(), 3);
        assertEquals(entityManager.getAllEntities(Email.class).size(), 2);
        assertEquals(entityManager.getAllEntities(PhoneNumber.class).size(), 2);

        assertEquals(entityManager.getAllComponents(Name.class).size(), 3);
        assertEquals(entityManager.getAllComponents(Email.class).size(), 2);
        assertEquals(entityManager.getAllComponents(PhoneNumber.class).size(), 2);

        assertEquals(entityManager.getAllComponents(contact1).size(), 2);
        assertEquals(entityManager.getAllComponents(contact2).size(), 2);
        assertEquals(entityManager.getAllComponents(contact3).size(), 3);

        assertEquals(entityManager.hasComponent(contact1, Name.class), true);
        assertEquals(entityManager.hasComponent(contact2, PhoneNumber.class), true);
        assertEquals(entityManager.hasComponent(contact2, Email.class), false);
        assertEquals(entityManager.hasComponent(contact3, Email.class), true);

        entityManager.removeComponent(contact1, Name.class);
        assertEquals(entityManager.getAllEntities(Name.class).size(), 2);

        Name name = entityManager.getComponent(contact2, Name.class);
        assertEquals(name.name, "Contact 2");
    }

}