package com.notnotme.entcomp;

public interface UpdatableSystem extends System {

    void update(EntityManager entityManager, long time);

}
