package com.notnotme.entcomp;

import java.util.Collection;
import java.util.HashMap;

public final class SystemManager {

    private HashMap<Class<? extends System>, System> mSystems;

    public SystemManager() {
        mSystems = new HashMap<>();
    }

    public void addSystem(System system) {
        mSystems.put(system.getClass(), system);
    }

    public void removeSystem(Class<? extends System> system) {
        System result = mSystems.remove(system);
        if (result == null) {
            throw new RuntimeException("Can't find system " + system);
        }
    }

    public <T extends System> T getSystem(Class<T> system) {
        T result = system.cast(mSystems.get(system));
        if (result == null) {
            throw new RuntimeException("Can't find system " + system);
        }

        return result;
    }

    public int getCount() {
        return mSystems.size();
    }

    public Collection<? extends System> getSystems() {
        return mSystems.values();
    }

}
