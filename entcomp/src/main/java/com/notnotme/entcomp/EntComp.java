package com.notnotme.entcomp;

public final class EntComp {

    private final EntityManager mEntityManager;
    private final SystemManager mSystemManager;

    public EntComp() {
        mEntityManager = new EntityManager();
        mSystemManager = new SystemManager();
    }

    public void initialize() {
        for (System system : mSystemManager.getSystems()) {
            system.onStart(mEntityManager);
        }
    }

    public void destroy() {
        for (System system : mSystemManager.getSystems()) {
            system.onStop(mEntityManager);
        }
    }

    public void update() {
        long time = java.lang.System.currentTimeMillis();
        for (System system : mSystemManager.getSystems()) {
            if (system instanceof UpdatableSystem) {
                ((UpdatableSystem)system).update(mEntityManager, time);
            }
        }
    }

    public EntityManager getEntityManager() {
        return mEntityManager;
    }

    public SystemManager getSystemManager() {
        return mSystemManager;
    }

}
