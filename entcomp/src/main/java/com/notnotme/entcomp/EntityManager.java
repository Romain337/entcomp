package com.notnotme.entcomp;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public final class EntityManager {

    private final ArrayList<UUID> mEntities;
    private final HashMap<Class<? extends Component>, HashMap<UUID, Component>> mStores;

    public EntityManager() {
        mEntities = new ArrayList<>();
        mStores = new HashMap<>();
    }

    public UUID createEntity() {
        UUID uuid = UUID.randomUUID();
        if (mEntities.contains(uuid)) {
            throw new RuntimeException("Duplicate entity id");
        }

        mEntities.add(uuid);
        return uuid;
    }

    public void removeEntity(UUID entity) {
        synchronized (mStores) {
            for (HashMap<UUID, Component> store : mStores.values()) {
                store.remove(entity);
            }
            mEntities.remove(entity);
        }
    }

    public void addComponent(UUID entity, Component component) {
        synchronized (mStores) {
            HashMap<UUID, Component> store = mStores.get(component.getClass());

            if (store == null) {
                store = new HashMap<>();
                mStores.put(component.getClass(), store);
            }

            store.put(entity, component);
        }
    }

    public void removeComponent(UUID entity, Class<? extends Component> componentType) {
        synchronized (mStores) {
            HashMap<UUID, Component> store = mStores.get(componentType);
            if (store == null) {
                throw new RuntimeException("No entities with component of type " + componentType);
            }

            Component result = store.remove(entity);
            if (result == null) {
                throw new RuntimeException("Can't find component " + componentType + " for entity " + entity);
            }
        }
    }

    public <T extends Component> T getComponent(UUID entity, Class<T> componentType) {
        synchronized (mStores) {
            HashMap<UUID, ? super Component> store = mStores.get(componentType);
            if (store == null) {
                throw new RuntimeException("No entities with component of type " + componentType);
            }

            T result = componentType.cast(store.get(entity));
            if (result == null) {
                throw new RuntimeException("Can't find component " + componentType + " for entity " + entity);
            }

            return result;
        }
    }

    public boolean hasComponent(UUID entity, Class<? extends Component> componentType) {
        synchronized (mStores) {
            HashMap<UUID, ? super Component> store = mStores.get(componentType);
            return store != null && store.containsKey(entity);
        }
    }

    public ArrayList<Component> getAllComponents(UUID entity) {
        synchronized (mStores) {
            ArrayList<Component> components = new ArrayList<>();
            for (HashMap<UUID, Component> store : mStores.values()) {
                if (store == null) continue;

                Component component = store.get(entity);
                if (component != null) {
                    components.add(component);
                }
            }

            return components;
        }
    }

    public Collection<? super Component> getAllComponents(Class<? extends Component> componentType) {
        synchronized (mStores) {
            HashMap<UUID, ? super Component> store = mStores.get(componentType);
            if (store == null) return new ArrayList<>();
            return store.values();
        }
    }

    public Set<UUID> getAllEntities(Class<? extends Component> componentType) {
        synchronized (mStores) {
            HashMap<UUID, ? extends Component> store = mStores.get(componentType);
            if (store == null) return new HashSet<>();
            return store.keySet();
        }
    }

    public int getCount() {
        return mEntities.size();
    }

}
