package com.notnotme.entcomp;

public interface System {

    void onStart(EntityManager entityManager);
    void onStop(EntityManager entityManager);
    String getName();

}
